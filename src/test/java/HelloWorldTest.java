import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HelloWorldTest {

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void sayHello() {
        HelloWorld helloWorld = new HelloWorld();

        Assert.assertEquals("Something went wrong!", "Hello world!", helloWorld.sayHello());
    }
}